import { useRef, useState } from "react";
import { Drawer, Form, Button, Col, Row, Input, Select, Divider, Spin, notification } from "antd";
import Upload from './upload';
import { LoadingOutlined } from "@ant-design/icons";

const { Option } = Select;

const ComponentDrawer = ({fields, openDrawer, setOpenDrawer, data, create, update}) => {

    const [loading, setLoading] = useState(false);
    const formRef = useRef(null);
    let cols = [];

    const openNotification = (type, message, description, placement, icon) => {
        notification[type]({
            message,
            description,
            placement,
            icon
        });
    };

    const onClose = () => {
        setOpenDrawer(false)
        formRef.current.resetFields()
    };

    const callback = ({loading, error, message, response, done}) => {
        setLoading(false)
        if(done && !loading){
            if(!error){
                openNotification('success', message, response.message, 'bottomRight')
                onClose()
            } else {
                openNotification('error', message, response.message, 'bottomRight')
            }
        }
    }

    const onFinish = (values) => {
        setLoading(true)
        if(!data){
            create(values, callback)
        } else {
            update(values, callback)
        }
    };

    const GetComponentFilter = (field, index) => {
        
        if(index % 2 === 0){
            cols = []
        }

        switch (field.type) {
            case 'text':
                cols.push(
                    <Col span={12}>
                        <Form.Item
                            key={field.key}
                            label={field.title}
                            name={field.dataIndex}
                        >
                            <Input
                                key={`input-${index}`}
                                placeholder={field.placeholder}
                                disabled={!field.isEdit}
                                
                            />
                        </Form.Item>
                    </Col>
                )
            break;
            case 'select':
            cols.push(
                <Col span={12}>
                <Form.Item
                    key={field.key}
                    label={field.title}
                    name={field.dataIndex}
                >
                    <Select
                        key={`select-${index}`}
                        optionFilterProp={field.optionFilterProp}
                        optionLabelProp={field.optionLabelProp}
                        placeholder={field.placeholder}
                        filterOption={field.filterOption}
                        showSearch={field.showSearch}
                        allowClear={field.allowClear}
                        showArrow={field.showArrow}
                        onChange={v => field.functions.onChange(v)}
                        onSearch={() => field.functions.onSearch()}
                        onFocus={() => field.functions.onFocus()}
                        onBlur={v => field.functions.onBlur(v)}
                        style={field.style}
                        rules={field.rules}                    
                    >
                    {
                        field.options.map((option, i) => {
                            return(
                                <Option
                                    key={`select-{index}-option-${i}`}
                                    value={option.value}>{option.label}
                                </Option>
                            )
                        })
                    }
                    </Select>
                </Form.Item>
            </Col>

            )
            break;
            default:
                return;
        }

        if(index % 2 === 1) {
            return(
                <Row gutter={16}>
                    {cols}
                </Row>
            )
        }
    }

    return (
        <Drawer
            title="Create a new account"
            width={720}
            onClose={onClose}
            visible={openDrawer}
            bodyStyle={{ paddingBottom: 80 }}
        >
            <Spin size="default" spinning={loading} indicator={<LoadingOutlined style={{fontSize: 24 }} spin />} tip="Loading....">
                <Form
                    ref={formRef}
                    layout="vertical"
                    hideRequiredMark
                    fields={data}
                    onFinish={onFinish}
                >
                    {
                        fields.map((field, index) => {
                            return(
                                GetComponentFilter(field, index)
                            )
                        })
                    }
                    <div style={{padding:'24px 0px'}}>
                        <Upload/>
                    </div>
                    <Row gutter={16} justify="end" align="bottom">
                        <Divider/>
                        <Col span={3}>
                            <Button color="red" htmlType="button" onClick={() => onClose()}>
                                Cancel
                            </Button>
                        </Col>
                        <Col span={3}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </Spin>
        </Drawer>
    );
}

export default ComponentDrawer;
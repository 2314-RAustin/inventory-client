import {Form, Row, Col, Input, Select} from 'antd';

export default ({filters}) => {
    const { Option } = Select;

    const GetComponentFilter = (filter, index) => {
        switch (filter.type) {
            case 'text':
            return (
                <Input key={`input-${index}`} placeholder={filter.placeholder}/>
            )
            case 'select':
            return(
                <Select
                    key={`select-${index}`}
                    optionFilterProp={filter.optionFilterProp}
                    optionLabelProp={filter.optionLabelProp}
                    placeholder={filter.placeholder}
                    mode={filter.mode}
                    filterOption={filter.filterOption}
                    showSearch={filter.showSearch}
                    allowClear={filter.allowClear}
                    showArrow={filter.showArrow}
                    onChange={v => filter.functions.onChange(v)}
                    onSearch={() => filter.functions.onSearch()}
                    onFocus={() => filter.functions.onFocus()}
                    onBlur={v => filter.functions.onBlur(v)}
                    style={filter.style}
                    rules={filter.rules}
                >
                {
                    filter.options.map((option, i) => {
                        return(
                            <Option key={`select-{index}-option-${i}`} value={option.value}>{option.label}</Option>
                        )
                    })
                }
                </Select>
            )
        }
    }

    return(
        <Row gutter={24} key='form-1-row-1'>
        {
            filters.map((filter, index) => {
                return(
                    filter.isFilter &&
                    <Col span={filter.span} key={`form-1-row-1-col-${index}`}>
                        <Form.Item
                            key={`form-1-row-1-col-${index}-form.item-${index}`}
                            label={filter.title}
                            name={filter.dataIndex}
                        >
                        {
                            GetComponentFilter(filter, index)
                        }
                        </Form.Item>
                    </Col>
                )
            })
        }
        </Row>
    )
}
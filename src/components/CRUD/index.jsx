import {Form, Table} from 'antd';
import Buttons from './button';
import Filters from './filters'
import Drawer from './drawer';
import './styles.css';

export default ({dataSource, columns, buttons, onFinish, loading, formRef, openDrawer, setOpenDrawer, dataDrawer, create, update}) => {

        const [form] = Form.useForm();

        return (
            <>
                <Form
                    key='form-1'
                    form={form}
                    ref={formRef}
                    name="advanced_search"
                    className="ant-advanced-search-form"
                    onFinish={onFinish}
                >
                    <Filters filters={columns}/>
                    <Buttons buttons={buttons} form={form}/>
                </Form>
                <Table
                    loading={loading}
                    className="search-result-list"
                    columns={columns}
                    dataSource={dataSource}
                    scroll={{ x: 2000, y: '70%' }}
                    sticky
                />
                <Drawer
                    fields={columns}
                    openDrawer={openDrawer}
                    setOpenDrawer={setOpenDrawer}
                    data={dataDrawer}
                    create={create}
                    update={update}
                />
            </>
        )
};
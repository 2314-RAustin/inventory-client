import {Row, Col, Button, Dropdown, Menu} from 'antd';
import { DownOutlined } from "@ant-design/icons";

const { SubMenu } = Menu;
export default ({buttons}) => {

    const menu = (menus) =>  {
        return(
            <Menu>
                {
                    menus.map((menu, index) => {
                        return(
                            <SubMenu
                                key={index}
                                title={menu.title}
                            >
                                {
                                    menu.sub_menus.map((sub_menu, index) => {
                                        return(
                                            <Menu.Item
                                                key={index}
                                                onClick={sub_menu.onClick}
                                                icon={sub_menu.icon}
                                            >
                                                { sub_menu.title}
                                            </Menu.Item>
                                        )
                                    })
                                }
                            </SubMenu>
                        )
                    })
                }
            </Menu>
        )
    }

    const GetComponentAction = (button, index) => {
        switch (button.element) {
            case 'button':
            return(
                <Button
                    key={`${button.key}-${index}`}
                    type={button.type}
                    htmlType={button.htmlType}
                    style={button.style}
                    icon={button.icon}
                    shape={button.shape}
                    onClick={() => {
                        button.functions && button.functions.length > 0 &&
                        button.functions.map(func => {
                            func();
                        })
                    }}
                >
                    {button.label}
                </Button>
            )
            case 'dropdown':
                return(
                    <Dropdown overlay={menu(button.menu)}>
                        <Button 
                            key={`${button.key}-${index}`}
                            type={button.type}
                            htmlType={button.htmlType}
                            style={button.style}
                            icon={button.icon}
                            onClick={(e) => e.preventDefault()}
                        >
                            Export <DownOutlined />
                        </Button>
                    </Dropdown>
                )
        }
    }

    return(
        <Row key='form-1-row-2'>
            <Col
                key={'form-1-row-2-col-1'}
                span={24}
                style={{
                    textAlign: 'right',
                }}
            >
            {
                buttons.map((button, index) => {
                    return(
                        GetComponentAction(button, index)
                    )
                })
            }
            </Col>
        </Row>
    )
}
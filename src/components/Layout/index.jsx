import { BrowserRouter as Router} from "react-router-dom";
import {useState} from 'react';
import { Layout } from 'antd';
import {Provider} from 'react-redux';
import store from '../../redux/store';
import Header from './header';
import Sider from './sider';
import Content from './content';
import Footer from './footer';
import './styles.css'

export default () => {
    const [collapsed, setCollapsed] = useState(true);
    const onCollapse = collapsed => {
        setCollapsed(collapsed)
    };

    return(
        <Provider store={store}>
            <Router>
                <Layout style={{ minHeight: '100vh' }}>
                    <Sider collapsed={collapsed} onCollapse={onCollapse}/>
                    <Layout className='site-layout'>
                        <Header collapsed={collapsed} onCollapse={onCollapse}/>
                        <Content/>
                        <Footer/>
                    </Layout>
                </Layout>
            </Router>
        </Provider>
    )
}
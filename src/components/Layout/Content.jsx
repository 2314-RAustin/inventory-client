import { Route } from 'react-router-dom'
import { Layout, Breadcrumb } from 'antd';
import Pages from '../../pages';
import './styles.css';

const { Content } = Layout;

const accRoutes = (routes, acc = [] , parentPath = null ) => {
    for( let i = 0 ; i < routes.length ; i++ ) {
        let { key, props:{path, exact}, component, subs} = routes[i];
        path = parentPath ? `${parentPath}${path}` : path
        acc.push({
            key,
            exact,
            path,
            component
        })
        if(subs){
            accRoutes(subs, acc, path )
        }
    }
    return acc
}

const Renderer = () => {
    let  xyz = accRoutes(Pages)
    return xyz.map( (o , i )=> {
        return (
            <Route {...o}/>
        )
    })
}

export default () => {
    return(
        <Content style={{ margin: '0 16px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>User</Breadcrumb.Item>
                <Breadcrumb.Item>Bill</Breadcrumb.Item>
            </Breadcrumb>
            <div className='site-layout-background' style={{ padding: 24, minHeight: '90%' }}>
                {Renderer()}
            </div>
        </Content>
    )
}
import { createElement } from 'react';
import { Affix, Layout } from 'antd';
import { MenuUnfoldOutlined,MenuFoldOutlined } from '@ant-design/icons';
import './styles.css';

const { Header } = Layout;

export default ({collapsed, onCollapse}) => {
    return(
        <Header className='site-layout-background' style={{ padding: 0 }}>
        {
            createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                className:'trigger',
                onClick: () => onCollapse(!collapsed),
            })
        }
        </Header>
    )
}
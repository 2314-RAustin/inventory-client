import { Layout, Menu } from 'antd';
import { NavLink } from 'react-router-dom';
import { BankOutlined, BlockOutlined, BuildOutlined, HomeOutlined, LoginOutlined } from '@ant-design/icons';
import './home.css';

const { Sider } = Layout;
const { SubMenu } = Menu;

export default ({collapsed, onCollapse, menu:{properties_menu, options_menu}}) => {

    const Icon = {
        'LoginOutlined': <LoginOutlined />,
        'HomeOutlined': <HomeOutlined />,
        'BankOutlined': <BankOutlined />,
        'TeamOutlined': <BlockOutlined />,
        'FileOutlined': <BuildOutlined />,
    }

    const getMenu = Object.entries(options_menu).map((option) => {
        let menuAll = [];
        if(!option[1]['subs']){
            menuAll.push(
                <Menu.Item key={option[1]['key']} icon={Icon[option[1]['props']['icon']]}>
                    <NavLink replace to={option[1]['props']['path']}>{option[1]['props']['title']}</NavLink>
                </Menu.Item>
            )
        } else {
            let childrens = option[1]['subs'];
            let menuChilds = [];
            for (const child of childrens) {
                const path =`${option[1]['props']['path']}${child['props']['path']}`;
                menuChilds.push(
                    <Menu.Item key={child['key']} icon={Icon[child['props']['icon']]}>
                        <NavLink replace to={path}>{child['props']['title']}</NavLink>
                    </Menu.Item>
                )
            }

            menuAll.push(
                <SubMenu key={option[1]['key']} icon={Icon[option[1]['props']['icon']]} title={option[1]['props']['title']}>
                {
                    menuChilds
                }
                </SubMenu>
            )
        }

        return menuAll
      });

    return (
        <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
            <div className='logo' />
            <Menu theme={properties_menu.theme} defaultSelectedKeys={properties_menu.defaultSelectedKeys} mode={properties_menu.mode}>
                {getMenu}
            </Menu>
        </Sider>
    )
}
import Sider from './sider'
import Pages from '../../../pages';

export default ({collapsed, onCollapse}) => {

    let menu = {
        'properties_menu': {
            theme:"dark",
            defaultSelectedKeys: ['1'],
            mode:"inline"
        },
        'options_menu':Pages
    }

    return (
        <Sider collapsed={collapsed} onCollapse={onCollapse} menu={menu}/>
    )
}
import Login from './login'
import Home from './home'
import * as Empresas from './empresas';

export default [
    {
        key: 'login',
        props: {
            path: '/',
            title: 'login',
            exact: true,
            icon: 'LoginOutlined',
        },
        component: Login,
    },
    {
        key: 'home',
        props: {
            path: '/home',
            title: 'home',
            exact: true,
            icon: 'HomeOutlined',
        },
        component: Home,
    },
    {
        key: 'empresas',
        props: {
            path: '/empresas',
            title: 'empresas',
            exact: true,
            icon: 'BankOutlined',
        },
        subs: [
            {
                key: 'convenidas',
                props: {
                    path: '/convenidas',
                    title: 'Convenidas',
                    exact: true,
                    icon: 'TeamOutlined'
                },
                component: Empresas.Convenidas
            },
            {
                key: 'vinculadas',
                props: {
                    path: '/vinculadas',
                    title: "Vinculadas",
                    exact: true,
                    icon: 'FileOutlined',
                },
                component: Empresas.Vinculadas
            }
        ]
    }
]
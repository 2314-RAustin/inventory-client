import {notification} from 'antd'
import {connect} from 'react-redux';
import action_login from '../../redux/actions/login'
import Login from './login';

const MainLogin = (props) => {
    const onFinish = (credentials) => {
        
        props.action_login({
            credentials, callback: (err = true, message = 'Error Application') => {
                if(!err){
                    notification['success']({
                        message: 'Success',
                        description: message,
                    });
                    props.history.push('/Home')
                } else {
                    notification['error']({
                        message: 'Error',
                        description: message,
                    });
                }
            }
        });
    }

    const onFinishFailed = (v) => {
        // console.log('Failed:', v);
    };


    return(
        <Login onFinish={onFinish} onFinishFailed={onFinishFailed} props={props}/>
    )
}

const mapStateToProps = (state) => {
    return {
        login: state.login
    }
}

const mapDispatchToProps = {
    action_login
}

export default connect(mapStateToProps, mapDispatchToProps)(MainLogin);
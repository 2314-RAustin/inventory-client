import {connect} from 'react-redux';
import loginAction from '../../redux/actions/login'
import Home from './home';

const MainHome = () => {
    return(
        <Home/>
    )
}

const mapStateToProps = (state) => {
    return {
        login: state.login
    }
}

const mapDispatchToProps = {
    loginAction
}

export default connect(mapStateToProps, mapDispatchToProps)(MainHome);
import { createRef, useEffect, useState } from 'react';
import {useSelector} from 'react-redux';
import Crud from '../../../components/crud';
import buttons from './buttons';
import columns from './columns';
import actions from './actions';

const Vinculdas = () => {

    const formRef = createRef();
    const [openDrawer, setOpenDrawer] = useState(false);
    const [data, setData] = useState({});
    const {read_company_data, read_company_loading} = useSelector(state => state.read_company_response);
    const {
        onFinish,
        onChange,
        onBlur,
        onFocus,
        onSearch,
        handleDelete,
        drawerVisible,
        handleResetTable,
        handleClearFilters,
        read,
        create,
        update,
        del,
        export_all_csv,
        export_all_pdf,
        export_all_xslx,
        export_filter_csv,
        export_filter_pdf,
        export_filter_xslx,
    } = actions({formRef, setOpenDrawer, setData});

    const crud_buttons = buttons({
        handleResetTable,
        handleClearFilters,
        drawerVisible,
        export_all_csv,
        export_all_pdf,
        export_all_xslx,
        export_filter_csv,
        export_filter_pdf,
        export_filter_xslx
    })

    const crud_columns = columns({
        onChange,
        onBlur,
        onFocus,
        onSearch,
        handleDelete,
        drawerVisible,
        del
    })

    useEffect(() => {
        read()
    },[])

    return(
        <Crud
            loading={read_company_loading}
            dataSource={read_company_data}
            columns={crud_columns}
            buttons={crud_buttons}
            onFinish={onFinish}
            formRef={formRef}
            openDrawer={openDrawer}
            setOpenDrawer={setOpenDrawer}
            dataDrawer={data}
            create={create}
            update={update}
        />
    )
}

export default Vinculdas;
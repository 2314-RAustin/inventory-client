import { SearchOutlined, DownloadOutlined, ClearOutlined, PlusCircleOutlined, FileExcelOutlined, FilePdfOutlined, FileTextOutlined  } from '@ant-design/icons';

const Buttons = ({
    handleResetTable,
    handleClearFilters,
    drawerVisible,
    export_all_csv,
    export_all_pdf,
    export_all_xslx,
    export_filter_csv,
    export_filter_pdf,
    export_filter_xslx
}) => [
    {
        key: 1,
        element: 'button',
        type:'primary',
        htmlType:'submit',
        style:{margin: '0 4px'},
        label:'Search',
        icon: <SearchOutlined />,
        shape:"round"
    },
    {
        key: 2,
        element: 'button',
        type:'primary',
        htmlType:'button',
        label:'Clear',
        style:{margin: '0 4px'},
        icon: <ClearOutlined />,
        shape:"round",
        functions: [
            handleResetTable,
            handleClearFilters
        ]
    },
    {
        key: 3,
        element: 'dropdown',
        type:'primary',
        htmlType:'submit',
        style:{margin: '0 4px'},
        label:'Export',
        icon: <DownloadOutlined />,
        shape:"round",
        menu:[{
            title: 'Todo', sub_menus:[
                {title: 'csv', onClick:export_all_csv, icon: <FileTextOutlined />},
                {title: 'pdf', onClick:export_all_pdf, icon: <FilePdfOutlined />},
                {title: 'xslx', onClick:export_all_xslx, icon: <FileExcelOutlined/>},
            ]
        },{
            title: 'Filtrado', sub_menus:[
                {title: 'csv', onClick:export_filter_csv, icon: <FileTextOutlined />},
                {title: 'pdf', onClick:export_filter_pdf, icon: <FilePdfOutlined />},
                {title: 'xslx', onClick:export_filter_xslx, icon: <FileExcelOutlined/>},
            ]
        }]
    },
    {
        key: 4,
        element: 'button',
        type:'primary',
        htmlType:'button',
        label:'Agregar empresa',
        style:{margin: '0 4px'},
        shape:"round",
        icon: <PlusCircleOutlined />,
        functions: [
            drawerVisible
        ]
    },
]

export default Buttons;
import {useDispatch} from 'react-redux'
import read from '../../../redux/actions/empresas/vinculadas/crud/read'
import create from '../../../redux/actions/empresas/vinculadas/crud/create'
import update from '../../../redux/actions/empresas/vinculadas/crud/update'
import del from '../../../redux/actions/empresas/vinculadas/crud/delete'
import export_all_csv from '../../../redux/actions/empresas/vinculadas/export/all/csv'
import export_all_pdf from '../../../redux/actions/empresas/vinculadas/export/all/pdf'
import export_all_xslx from '../../../redux/actions/empresas/vinculadas/export/all/xslx'
import export_filter_csv from '../../../redux/actions/empresas/vinculadas/export/filter/csv'
import export_filter_pdf from '../../../redux/actions/empresas/vinculadas/export/filter/pdf'
import export_filter_xslx from '../../../redux/actions/empresas/vinculadas/export/filter/xslx'

import store from '../../../redux/store'
import { useState } from 'react'

const Actions = ({formRef, setOpenDrawer, setData}) => {

    const dispatch = useDispatch();
    const [filtersApply, setFiltersApply] = useState(undefined)
    
    const getDoneStateReducer = (type_state, callback) => {
        let currentValue;
        function handleChange() {
            let previousValue = currentValue
            currentValue = store.getState()[type_state]
            if (previousValue !== currentValue) {
                if(!currentValue.loading && currentValue.done){
                    console.log('Llego aqui')
                    callback({...currentValue})
                    unsuscribe();
                }
            }
        }
        const unsuscribe = store.subscribe(handleChange)
    }

    return {
        onBlur: () => console.log('blur'),
        onFocus: () => console.log('focus'),
        onSearch: v => console.log('search:', v),
        onChange: v => console.log('selected ', v),
        handleDelete: v => console.log('Delete values of form: ', v),
        handleClearFilters: () => formRef.current.resetFields(),
        handleResetTable: () => {
            dispatch(read())
            setFiltersApply(undefined)
        },
        onFinish: v => {
            dispatch(read(v))
            setFiltersApply(v)
        },
        drawerVisible: (v) => {
            setData(v)
            setOpenDrawer(true)
        },
        read: () => {
            dispatch(read())
            setFiltersApply(undefined)
        },
        create: (v, callback) => {
            dispatch(create(v))
            getDoneStateReducer('create_company_response', callback)
            if(filtersApply){
                dispatch(read(filtersApply))
            } else {
                dispatch(read())
            }
            
        },
        update: (v, callback) => {
            dispatch(update(v))
            getDoneStateReducer('update_company_response', callback)
            if(filtersApply){
                dispatch(read(filtersApply))
            } else {
                dispatch(read())
            }
        },
        del: (v, callback) => {
            dispatch(del(v))
            getDoneStateReducer('delete_company_response', callback)
            if(filtersApply){
                dispatch(read(filtersApply))
            } else {
                dispatch(read())
            }
        },
        export_all_csv: () => dispatch(export_all_csv()),
        export_all_pdf: () => dispatch(export_all_pdf()),
        export_all_xslx: () => dispatch(export_all_xslx()),
        export_filter_csv: () => dispatch(export_filter_csv(filtersApply)),
        export_filter_pdf: () => dispatch(export_filter_pdf(filtersApply)),
        export_filter_xslx: () => dispatch(export_filter_xslx(filtersApply))
    }
}

export default Actions;
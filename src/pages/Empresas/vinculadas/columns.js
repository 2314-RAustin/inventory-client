import { Popconfirm, notification, Tag } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

export default ({ onChange, onBlur, onFocus, onSearch, drawerVisible, del }) => {

    const openNotification = (type, message, description, placement, icon) => {
        notification[type]({
            message,
            description,
            placement,
            icon
        });
    };

    const callback = ({ loading, error, message, response, done }) => {
        if (done && !loading) {
            if (!error) {
                openNotification('success', message, response.message, 'bottomRight')
            } else {
                openNotification('error', message, response.message, 'bottomRight')
            }
        }
    }

    return [
        {
            key: 1,
            title: 'ID Company',
            dataIndex: 'id_company',
            type: 'text',
            placeholder: 'ID Company',
            width: 50,
            span: 5,
            fixed: 'left',
            isFilter: false,
            isEdit: false,
            rules: [{
                required: false,
                message: 'ID Empresa requerido',
            }]
        },
        {
            key: 2,
            title: 'Empresa',
            dataIndex: 'empresa',
            type: 'text',
            placeholder: 'Ingrese empresa',
            width: 150,
            span: 4,
            fixed: 'left',
            isFilter: true,
            isEdit: true,
            sorter: (a, b) => a.empresa.localeCompare(b.empresa),
            rules: [{
                required: false,
                message: 'Empresa requerido',
            }]
        },
        {
            key: 3,
            title: 'Contacto',
            dataIndex: 'contacto',
            type: 'text',
            placeholder: 'Ingrese contacto',
            width: 80,
            span: 4,
            isFilter: false,
            isEdit: true,
            rules: [{
                required: false,
                message: 'Contacto requerido',
            }],
        },
        {
            key: 4,
            title: 'Telefono',
            dataIndex: 'telefono',
            type: 'text',
            placeholder: 'Ingrese telefono',
            width: 80,
            span: 4,
            isFilter: false,
            isEdit: true,
            rules: [{
                required: false,
                message: 'Telefono requerido',
            }],
        },
        {
            key: 5,
            title: 'Correo electronico',
            dataIndex: 'correo_electronico',
            type: 'text',
            placeholder: 'Ingrese correo electronico',
            width: 100,
            span: 5,
            isFilter: false,
            isEdit: true,
            rules: [{
                required: false,
                message: 'Correo electronico requerido',
            }],
        },
        {
            key: 6,
            title: 'Giro',
            dataIndex: 'giro',
            type: 'select',
            placeholder: 'Seleccione giro',
            width: 80,
            span: 4,
            isFilter: true,
            isEdit: true,
            rules: [{
                required: false,
                message: 'Giro requerido',
            }],
            showSearch: true,
            allowClear: true,
            showArrow: true,
            optionisFilterProp: 'children',
            mode: 'multiple',
            optionLabelProp: "label",
            options: [
                { value: 'Industrial', label: 'Industrial', },
                { value: 'Comercio', label: 'Comercio' },
                { value: 'Servicio', label: 'Servicio' },
                { value: 'Aeroespacial', label: 'Aeroespacial' }
            ],
            functions: {
                onChange, onBlur, onFocus, onSearch
            }
        },
        {
            key: 7,
            title: 'Convenio',
            dataIndex: 'convenio',
            type: 'select',
            placeholder: 'Seleccione convenio',
            width: 50,
            span: 4,
            isFilter: true,
            isEdit: true,
            rules: [{
                required: false,
                message: 'Convenio requerido',
            }],
            showSearch: true,
            allowClear: true,
            showArrow: true,
            sorter: true,
            optionisFilterProp: 'children',
            mode: 'multiple',
            optionLabelProp: "label",
            align: 'center',
            options: [
                { value: 'Si', label: 'Si', },
                { value: 'No', label: 'No' },
            ],
            functions: {
                onChange, onBlur, onFocus, onSearch
            },
            render: value => (
                <>
                    {
                        value.toUpperCase() === 'SI' &&
                        <Tag color='geekblue' key={1}>
                            {value.toUpperCase()}
                        </Tag>
                    }
                    {
                        value.toUpperCase() === 'NO' &&
                        <Tag color='volcano' key={2}>
                            {value.toUpperCase()}
                        </Tag>
                    }
                </>
            ),
        },
        {
            key: 8,
            title: 'Comentarios',
            dataIndex: 'comentarios',
            type: 'text',
            placeholder: 'Ingrese comentarios',
            width: 100,
            span: 5,
            isFilter: false,
            isEdit: true,
            rules: [{
                required: false,
                message: 'Comentarios requerido',
            }],
        },
        {
            key: 9,
            isFilter: false,
            isEdit: false,
            title: 'Action',
            dataIndex: 'operation',
            fixed: 'right',
            width: 50,
            render: (_, record) => [
                true ? (
                    <Popconfirm title="Sure to delete?" onConfirm={() => {
                        del(record, callback)
                    }}>
                        <a><DeleteOutlined /></a>
                    </Popconfirm>
                ) : null,
                <a><EditOutlined onClick={() => {

                    let data = [];
                    for (const [key, value] of Object.entries(record)) {
                        data.push({ name: [key], value })
                    }

                    drawerVisible(data)
                }} /></a>
            ]
        },
    ]
}


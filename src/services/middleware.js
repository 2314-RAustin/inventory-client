import axios from 'axios';
import config from '../config.json';
import { download_file } from '../helpers/download';
const mode = config.mode;
const {middleware:{api_url:{base_api_url,auth:{base_auth,signin,signup},users:{base_users,create_user}}}} = config[mode];

export const login = async (credentials) => {
    return new Promise((ready) => {
        if(credentials){
            axios.post(base_api_url + base_auth + signin, credentials, {
                headers: {'Content-Type': 'application/json'}
            })
            .then((response) => {ready(response.data)})
            .catch((error) => {
                if(error.response){
                    ready(error.response.data)
                }
                ready(error)
            })
        } else {
            ready("Error aplication code 1")
        }
    })
}

export const getData = (filters) => {
    return new Promise(async (ready) => {
        if(filters){

            if('empresa' in filters && (!filters['empresa'] || filters['empresa'].length === 0)){
                delete filters['empresa']
            }

            if('giro' in filters && (!filters['giro'] || filters['giro'].length === 0)){
                delete filters['giro']
            }

            if('convenio' in filters && (!filters['convenio'] || filters['convenio'].length === 0)){
                delete filters['convenio']
            }

            axios.request({
                method: 'GET',
                url: `http://localhost:2000/api/company`,
                headers: {'Content-Type': 'application/json'},
                params: filters,
            })
            .then((response) => {ready(response.data.response)})
            .catch((error) => {ready(error)})

        } else {
            axios.get('http://localhost:2000/api/company', {
                headers: {'Content-Type': 'application/json'}
            })
            .then((response) => {ready(response.data.response)})
            .catch((error) => {ready(error)})
        }
    })
}

export const createCompanyMiddleware = (data) => {
    return new Promise((resolve, reject) => {
        delete data['id_company']

        axios.request({
            method: 'POST',
            url: `http://localhost:2000/api/company`, data,
            headers: {'Content-Type': 'application/json'},
        })
        .then((response) => {
            let {error} = response.data;
            if(error){
                reject(response.data);
            } else {
                resolve(response.data)
            }
        })
        .catch((error) => {
            reject(error.response.data)
        })
    })
}

export const updateCompanyMiddleware = (data) => {
    return new Promise((resolve, reject) => {
        axios.request({
            method: 'PUT',
            url: `http://localhost:2000/api/company`, data,
            headers: {'Content-Type': 'application/json'},
        })
        .then((response) => {
            let {error} = response.data;
            if(error){
                reject(response.data);
            } else {
                resolve(response.data)
            }
        })
        .catch((error) => {
            reject(error.response.data)
        })
    })
}

export const deleteCompanyMiddleware = (data) => {
    return new Promise((resolve, reject) => {
        const {id_company, empresa} = data;
        axios.request({
            method: 'delete',
            url: `http://localhost:2000/api/company`,
            data: {id_company, empresa},
            headers: {'Content-Type': 'application/json'},
        })
        .then((response) => {
            let {error} = response.data;
            if(error){
                reject(response.data);
            } else {
                resolve(response.data)
            }
        })
        .catch((error) => {
            console.log('error ', error)
            reject(error.response.data)
        })
    })
}

export const companyExportAllCsvMiddleware = () => {
    return new Promise((resolve, reject) => {
        axios.request({
            responseType: 'blob',
            method: 'get',
            url: `http://localhost:2000/api/company/export/all/csv`,
            headers: {'Content-Type': 'application/json'},
        })
        .then(response => {
            let {error} = response.data;
            if(error){
                reject(response.data);
            } else {
                download_file(response)
                resolve(response.data)
            }
        })
        .catch(error => {
            console.log('error ', error)
            reject(error.response.data)
        })
    })
}

export const companyExportAllPdfMiddleware = () => {
    return new Promise((resolve, reject) => {
        axios.request({
            responseType: 'blob',
            method: 'get',
            url: `http://localhost:2000/api/company/export/all/pdf`,
            headers: {'Content-Type': 'application/json'},
        })
        .then((response) => {
            let {error} = response.data;
            if(error){
                reject(response.data);
            } else {
                download_file(response)
                resolve(response.data)
            }
        })
        .catch((error) => {
            console.log('error ', error)
            reject(error.response.data)
        })
    })
}

export const companyExportAllXslxMiddleware = () => {
    return new Promise((resolve, reject) => {
        axios.request({
            responseType: 'blob',
            method: 'get',
            url: `http://localhost:2000/api/company/export/all/xslx`,
            headers: {'Content-Type': 'application/json'},
        })
        .then(response => {
            let {error} = response.data;
            if(error){
                reject(response.data);
            } else {
                download_file(response)
                resolve(response.data)
            }
        })
        .catch(error => {
            console.log('error ', error)
            reject(error.response.data)
        })
    })
}

export const companyExportFilterCsvMiddleware = (filters) => {
    return new Promise((resolve, reject) => {

        if('empresa' in filters && (!filters['empresa'] || filters['empresa'].length === 0)){
            delete filters['empresa']
        }

        if('giro' in filters && (!filters['giro'] || filters['giro'].length === 0)){
            delete filters['giro']
        }

        if('convenio' in filters && (!filters['convenio'] || filters['convenio'].length === 0)){
            delete filters['convenio']
        }

        axios.request({
            responseType: 'blob',
            method: 'get',
            url: `http://localhost:2000/api/company/export/filter/csv`,
            headers: {'Content-Type': 'application/json'},
            params: filters,
        })
        .then(response => {
            let {error} = response.data;
            if(error){
                reject(response.data);
            } else {
                download_file(response)
                resolve(response.data)
            }
        })
        .catch(error => {
            console.log('error ', error)
            reject(error.response.data)
        })
    })
}

export const companyExportFilterPdfMiddleware = (data) => {
    return new Promise((resolve, reject) => {
        const {id_company, empresa} = data;
        axios.request({
            responseType: 'blob',
            method: 'get',
            url: `http://localhost:2000/api/company/export/filter/pdf`,
            data: {id_company, empresa},
            headers: {'Content-Type': 'application/json'},
        })
        .then((response) => {
            let {error} = response.data;
            if(error){
                reject(response.data);
            } else {
                download_file(response)
                resolve(response.data)
            }
        })
        .catch((error) => {
            console.log('error ', error)
            reject(error.response.data)
        })
    })
}

export const companyExportFilterXslxMiddleware = (filters) => {
    return new Promise(async (resolve, reject) => {
        if(filters){

            if('empresa' in filters && (!filters['empresa'] || filters['empresa'].length === 0)){
                delete filters['empresa']
            }

            if('giro' in filters && (!filters['giro'] || filters['giro'].length === 0)){
                delete filters['giro']
            }

            if('convenio' in filters && (!filters['convenio'] || filters['convenio'].length === 0)){
                delete filters['convenio']
            }

            axios.request({
                responseType: 'blob',
                method: 'get',
                url: `http://localhost:2000/api/company/export/filter/xslx`,
                headers: {'Content-Type': 'application/json'},
                params: filters,
            })
            .then(response => {
                let {error} = response.data;
                if(error){
                    reject(response.data);
                } else {
                    download_file(response)
                    resolve(response.data)
                }
            })
            .catch(error => {
                console.log('error ', error)
                reject(error.response.data)
            })

        }
    })
}
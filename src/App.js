import React, { Suspense } from 'react'
const LazyLoader = React.lazy(() => import('./components/layout'))

const App = () =>  {
  return (
    <Suspense fallback={<div>Loading... </div>}>
      <LazyLoader />
    </Suspense>
  )
}

export default App;
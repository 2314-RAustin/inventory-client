import {login as loginMiddleware} from '../../services/middleware'
import {type as login} from '../actions/login'
const defaultState = {}; 

function reducer (state = defaultState, {type, payload}) {
    switch (type) {
        case login:{
            let {credentials, callback} = payload;
            loginMiddleware(credentials)
            .then(resp => {
                if(resp.message){
                    callback(resp.error, resp.message);
                    return {login: resp.message}
                } else {
                    callback(true, resp)
                    return {login: resp}
                }
            })
            .catch(error => {
                callback(true, error)
                return {login: error}
            })
        }
        default:{
            return state;
        }
    }
}

export default reducer;
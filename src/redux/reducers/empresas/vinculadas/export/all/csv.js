import { COMPANY_EXPORT_ALL_CSV_REQUEST, COMPANY_EXPORT_ALL_CSV_SUCCESS, COMPANY_EXPORT_ALL_CSV_FAILURE } from '../../../../../actions/empresas/vinculadas/export/all/csv'
const defaultState = {
    loading: false,
    error: false,
    message: '',
    response: [],
    done: false,
};

const create = (state = defaultState, {type, payload}) => {
    switch (type) {
        case COMPANY_EXPORT_ALL_CSV_REQUEST:{
            return {...state, loading: true, error: false, message: 'Loading', response: payload}
        }
        case COMPANY_EXPORT_ALL_CSV_SUCCESS:{
            return {...state, loading: false, error: false, done: true, message: 'Success', response: payload}
        }
        case COMPANY_EXPORT_ALL_CSV_FAILURE:{
            return {...state, loading: false, error: true, done: true, message: 'Error', response: payload}
        }
        default:{
            return {...state, loading: false, message: 'default'};
        }
    }
}

export default create;
import { DELETE_COMPANY_REQUEST, DELETE_COMPANY_SUCCESS, DELETE_COMPANY_FAILURE } from '../../../../actions/empresas/vinculadas/crud/delete'
const defaultState = {
    delete: false,
    error: false,
    message: '',
    response: [],
    done: false,
};

const del = (state = defaultState, {type, payload}) => {
    switch (type) {
        case DELETE_COMPANY_REQUEST:{
            return {...state, delete: true, error: false, message: 'Loading', response: payload}
        }
        case DELETE_COMPANY_SUCCESS:{
            return {...state, delete: false, error: false, done: true, message: 'Success', response: payload}
        }
        case DELETE_COMPANY_FAILURE:{
            return {...state, delete: false, error: true, done: true,message: 'Error', response: payload}
        }
        default:{
            return {...state, delete: false, error: false, message: 'default'};
        }
    }
}

export default del;
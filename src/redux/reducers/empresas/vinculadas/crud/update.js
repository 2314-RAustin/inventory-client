import { UPDATE_COMPANY_REQUEST, UPDATE_COMPANY_SUCCESS, UPDATE_COMPANY_FAILURE } from '../../../../actions/empresas/vinculadas/crud/update'
const defaultState = {
    loading: false,
    error: false,
    message: '',
    response: [],
    done: false,
};

const create = (state = defaultState, {type, payload}) => {
    switch (type) {
        case UPDATE_COMPANY_REQUEST:{
            return {...state, loading: true, error: false, message: 'Loading', response: payload}
        }
        case UPDATE_COMPANY_SUCCESS:{
            return {...state, loading: false, error: false, done: true, message: 'Success', response: payload}
        }
        case UPDATE_COMPANY_FAILURE:{
            return {...state, loading: false, error: true, done: true,message: 'Error', response: payload}
        }
        default:{
            return {...state, loading: false, message: 'default'};
        }
    }
}

export default create;
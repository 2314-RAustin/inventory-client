import { READ_COMPANYS_FAILURE, READ_COMPANYS_REQUEST, READ_COMPANYS_SUCCESS, READ_FILTER_COMPANYS_SUCCESS } from '../../../../actions/empresas/vinculadas/crud/read'
const defaultState = {
    read_company_loading: false,
    read_company_error: false,
    read_company_message: '',
    read_company_data: [],
};

export default (state = defaultState, {type, payload}) => {
    switch (type) {
        case READ_COMPANYS_REQUEST:{
            return {...state, read_company_loading: true, read_company_error: false, read_company_message: 'Loading...', read_company_data: payload}
        }
        case READ_FILTER_COMPANYS_SUCCESS:{
            return {...state, read_company_loading: false, read_company_error: false, read_company_message: 'succes', read_company_data: payload}
        }
        case READ_COMPANYS_SUCCESS:{
            return {...state, read_company_loading: false, read_company_error: false, read_company_message: 'success', read_company_data: payload}
        }
        case READ_COMPANYS_FAILURE:{
            return {...state, read_company_loading: false, read_company_error: true, read_company_message: 'error', read_company_data: payload}
        }
        default:{
            return {...state, read_company_loading: false, read_company_error: false, read_company_message: 'default'};
        }
    }
}
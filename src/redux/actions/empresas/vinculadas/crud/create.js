import {createCompanyMiddleware} from '../../../../../services/middleware'

export const CREATE_COMPANY_REQUEST = "CREATE_COMPANY_REQUEST";
export const CREATE_COMPANY_SUCCESS = "CREATE_COMPANY_SUCCESS";
export const CREATE_COMPANY_FAILURE = "CREATE_COMPANY_FAILURE";

export const createCompanyRequest = (values) => {
    console.log(CREATE_COMPANY_REQUEST, ' COMPANY: ', values)
    return {
        type:CREATE_COMPANY_REQUEST,
        payload: 'loading...'
    }
}

export const createCompanySucces = (response) => {
    console.log(CREATE_COMPANY_SUCCESS, 'RESPUESTA:', response)
    return {
        type:CREATE_COMPANY_SUCCESS,
        payload: response
    }
}

export const createCompanyFailure = (error) => {
    console.log(CREATE_COMPANY_FAILURE, 'ERROR: ', error)
    return {
        type:CREATE_COMPANY_FAILURE,
        payload: error
    }
}

const createCompany = (values) => {
    return (dispatch) => {
        dispatch(createCompanyRequest(values));
        createCompanyMiddleware(values)
        .then(resp => dispatch(createCompanySucces(resp)))
        .catch(err => dispatch(createCompanyFailure(err)))
    }
}

export default createCompany;
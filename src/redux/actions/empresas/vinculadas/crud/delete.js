import {deleteCompanyMiddleware} from '../../../../../services/middleware'

export const DELETE_COMPANY_REQUEST = "DELETE_COMPANY_REQUEST";
export const DELETE_COMPANY_SUCCESS = "DELETE_COMPANY_SUCCESS";
export const DELETE_COMPANY_FAILURE = "DELETE_COMPANYFAILURE";

export const deleteCompanyRequest = (values) => {
    console.log(DELETE_COMPANY_REQUEST, ' COMPANY: ', values)
    return {
        type:DELETE_COMPANY_REQUEST
    }
}

export const deleteCompanySucces = (companys) => {
    console.log(DELETE_COMPANY_SUCCESS, ' RESPUESTA: ', companys)
    return {
        type:DELETE_COMPANY_SUCCESS,
        payload: companys
    }
}

export const deleteCompanyFailure = (error) => {
    console.log(DELETE_COMPANY_FAILURE, ' ERROR: ', error)
    return {
        type:DELETE_COMPANY_FAILURE,
        payload: error
    }
}

const deleteCompany = (values) => {
    return (dispatch) => {
        dispatch(deleteCompanyRequest(values));
        deleteCompanyMiddleware(values)
        .then(resp => dispatch(deleteCompanySucces(resp)))
        .catch(err => dispatch(deleteCompanyFailure(err)))
    }
}

export default deleteCompany;
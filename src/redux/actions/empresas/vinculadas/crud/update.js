import {updateCompanyMiddleware} from '../../../../../services/middleware'

export const UPDATE_COMPANY_REQUEST = "UPDATE_COMPANY_REQUEST";
export const UPDATE_COMPANY_SUCCESS = "UPDATE_COMPANY_SUCCESS";
export const UPDATE_COMPANY_FAILURE = "UPDATE_COMPANYFAILURE";

export const updateCompanyRequest = (values) => {
    console.log(UPDATE_COMPANY_REQUEST, ' COMPANY: ', values)
    return {
        type:UPDATE_COMPANY_REQUEST
    }
}

export const updateCompanySucces = (companys) => {
    console.log(UPDATE_COMPANY_SUCCESS, ' RESPUESTA: ', companys)
    return {
        type:UPDATE_COMPANY_SUCCESS,
        payload: companys
    }
}

export const updateCompanyFailure = (error) => {
    console.log(UPDATE_COMPANY_FAILURE, ' ERROR: ', error)
    return {
        type:UPDATE_COMPANY_FAILURE,
        payload: error
    }
}

const updateCompany = (values) => {
    return (dispatch) => {
        dispatch(updateCompanyRequest(values));
        updateCompanyMiddleware(values)
        .then(resp => dispatch(updateCompanySucces(resp)))
        .catch(err => dispatch(updateCompanyFailure(err)))
    }
}

export default updateCompany;
import {getData} from '../../../../../services/middleware'
export const READ_COMPANYS_REQUEST = "READ_COMPANYS_REQUEST";
export const READ_COMPANYS_SUCCESS = "READ_COMPANYS_SUCCESS";
export const READ_FILTER_COMPANYS_SUCCESS = 'READ_FILTER_COMPANYS_SUCCESS';
export const READ_COMPANYS_FAILURE = "READ_COMPANYS_FAILURE";

export const readCompanyRequest = (filters) => {
    console.log(READ_COMPANYS_REQUEST, ' FILTERS: ', filters)
    return {
        type:READ_COMPANYS_REQUEST
    }
}

export const readCompanysSuccess = (companys) => {
    console.log(READ_COMPANYS_SUCCESS, ' RESPUESTA: ', companys)
    return {
        type:READ_COMPANYS_SUCCESS,
        payload: companys
    }
}

export const readFilterCompanysSuccess = (companys) => {
    console.log(READ_FILTER_COMPANYS_SUCCESS, ' RESPUESTA: ', companys)
    return {
        type:READ_FILTER_COMPANYS_SUCCESS,
        payload: companys
    }
}

export const readCompanyFailure = (error) => {
    console.log(READ_COMPANYS_FAILURE, ' ERROR: ', error)
    return {
        type:READ_COMPANYS_FAILURE,
        payload: error
    }
}

const readCompany = (values) => {
    return (dispatch) => {
        dispatch(readCompanyRequest(values));
        if(values){
            getData(values)
            .then(resp => dispatch(readFilterCompanysSuccess(resp)))
            .catch(err => dispatch(readCompanyFailure(err)))
        } else {
            getData()
            .then(resp => dispatch(readCompanysSuccess(resp)))
            .catch(err => dispatch(readCompanyFailure(err)))
        }
    }
}

export default readCompany;
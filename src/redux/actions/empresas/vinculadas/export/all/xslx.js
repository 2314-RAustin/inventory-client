import {companyExportAllXslxMiddleware} from '../../../../../../services/middleware'

export const COMPANY_EXPORT_ALL_XSLX_REQUEST = "COMPANY_EXPORT_ALL_XSLX_REQUEST";
export const COMPANY_EXPORT_ALL_XSLX_SUCCESS = "COMPANY_EXPORT_ALL_XSLX_SUCCESS";
export const COMPANY_EXPORT_ALL_XSLX_FAILURE = "COMPANY_EXPORT_ALL_XSLX_FAILURE";

export const companyExportAllXslxRequest = () => {
    console.log(COMPANY_EXPORT_ALL_XSLX_REQUEST)
    return {
        type:COMPANY_EXPORT_ALL_XSLX_REQUEST,
        payload: 'loading...'
    }
}

export const companyExportAllXslxSucces = (response) => {
    console.log(COMPANY_EXPORT_ALL_XSLX_SUCCESS, 'RESPUESTA:', response)
    return {
        type:COMPANY_EXPORT_ALL_XSLX_SUCCESS,
        payload: response
    }
}

export const companyExportAllXslxFailure = (error) => {
    console.log(COMPANY_EXPORT_ALL_XSLX_FAILURE, 'ERROR: ', error)
    return {
        type:COMPANY_EXPORT_ALL_XSLX_FAILURE,
        payload: error
    }
}

const companyExportAllXslx = () => {
    return (dispatch) => {
        dispatch(companyExportAllXslxRequest());
        companyExportAllXslxMiddleware()
        .then(resp => dispatch(companyExportAllXslxSucces(resp)))
        .catch(err => dispatch(companyExportAllXslxFailure(err)))
    }
}

export default companyExportAllXslx;
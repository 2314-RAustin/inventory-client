import {companyExportAllPdfMiddleware} from '../../../../../../services/middleware'

export const COMPANY_EXPORT_ALL_PDF_REQUEST = "COMPANY_EXPORT_ALL_PDF_REQUEST";
export const COMPANY_EXPORT_ALL_PDF_SUCCESS = "COMPANY_EXPORT_ALL_PDF_SUCCESS";
export const COMPANY_EXPORT_ALL_PDF_FAILURE = "COMPANY_EXPORT_ALL_PDF_FAILURE";

export const companyExportAllPdfRequest = () => {
    console.log(COMPANY_EXPORT_ALL_PDF_REQUEST)
    return {
        type:COMPANY_EXPORT_ALL_PDF_REQUEST,
        payload: 'loading...'
    }
}

export const companyExportAllPdfSucces = (response) => {
    console.log(COMPANY_EXPORT_ALL_PDF_SUCCESS, 'RESPUESTA:', response)
    return {
        type:COMPANY_EXPORT_ALL_PDF_SUCCESS,
        payload: response
    }
}

export const companyExportAllPdfFailure = (error) => {
    console.log(COMPANY_EXPORT_ALL_PDF_FAILURE, 'ERROR: ', error)
    return {
        type:COMPANY_EXPORT_ALL_PDF_FAILURE,
        payload: error
    }
}

const companyExportAllPdf = () => {
    return (dispatch) => {
        dispatch(companyExportAllPdfRequest());
        companyExportAllPdfMiddleware()
        .then(resp => dispatch(companyExportAllPdfSucces(resp)))
        .catch(err => dispatch(companyExportAllPdfFailure(err)))
    }
}

export default companyExportAllPdf;
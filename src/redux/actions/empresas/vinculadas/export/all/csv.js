import {companyExportAllCsvMiddleware} from '../../../../../../services/middleware'

export const COMPANY_EXPORT_ALL_CSV_REQUEST = "COMPANY_EXPORT_ALL_CSV_REQUEST";
export const COMPANY_EXPORT_ALL_CSV_SUCCESS = "COMPANY_EXPORT_ALL_CSV_SUCCESS";
export const COMPANY_EXPORT_ALL_CSV_FAILURE = "COMPANY_EXPORT_ALL_CSV_FAILURE";

export const companyExportAllCsvRequest = () => {
    console.log(COMPANY_EXPORT_ALL_CSV_REQUEST)
    return {
        type:COMPANY_EXPORT_ALL_CSV_REQUEST,
        payload: 'loading...'
    }
}

export const companyExportAllCsvSucces = (response) => {
    console.log(COMPANY_EXPORT_ALL_CSV_SUCCESS, 'RESPUESTA:', response)
    return {
        type:COMPANY_EXPORT_ALL_CSV_SUCCESS,
        payload: response
    }
}

export const companyExportAllCsvFailure = (error) => {
    console.log(COMPANY_EXPORT_ALL_CSV_FAILURE, 'ERROR: ', error)
    return {
        type:COMPANY_EXPORT_ALL_CSV_FAILURE,
        payload: error
    }
}

const companyExportAllCsv = () => {
    return (dispatch) => {
        dispatch(companyExportAllCsvRequest());
        companyExportAllCsvMiddleware()
        .then(resp => dispatch(companyExportAllCsvSucces(resp)))
        .catch(err => dispatch(companyExportAllCsvFailure(err)))
    }
}

export default companyExportAllCsv;
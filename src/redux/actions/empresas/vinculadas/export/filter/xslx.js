import {companyExportFilterXslxMiddleware} from '../../../../../../services/middleware'
import {companyExportAllXslxMiddleware} from '../../../../../../services/middleware'

export const COMPANY_EXPORT_FILTER_XSLX_REQUEST = "COMPANY_EXPORT_FILTER_XSLX_REQUEST";
export const COMPANY_EXPORT_FILTER_XSLX_SUCCESS = "COMPANY_EXPORT_FILTER_XSLX_SUCCESS";
export const COMPANY_EXPORT_FILTER_XSLX_FAILURE = "COMPANY_EXPORT_FILTER_XSLX_FAILURE";

export const companyExportFilterXslxRequest = (values) => {
    console.log(COMPANY_EXPORT_FILTER_XSLX_REQUEST, ' FILTERS: ', values)
    return {
        type:COMPANY_EXPORT_FILTER_XSLX_REQUEST,
        payload: 'loading...'
    }
}

export const companyExportFilterXslxSucces = (response) => {
    console.log(COMPANY_EXPORT_FILTER_XSLX_SUCCESS, 'RESPUESTA:', response)
    return {
        type:COMPANY_EXPORT_FILTER_XSLX_SUCCESS,
        payload: response
    }
}

export const companyExportFilterXslxFailure = (error) => {
    console.log(COMPANY_EXPORT_FILTER_XSLX_FAILURE, 'ERROR: ', error)
    return {
        type:COMPANY_EXPORT_FILTER_XSLX_FAILURE,
        payload: error
    }
}

const companyExportFilterXslx = (values) => {
    return (dispatch) => {
        dispatch(companyExportFilterXslxRequest(values));
        if(!values){
            companyExportAllXslxMiddleware()
            .then(resp => dispatch(companyExportFilterXslxSucces(resp)))
            .catch(err => dispatch(companyExportFilterXslxFailure(err)))
        } else {
            companyExportFilterXslxMiddleware(values)
            .then(resp => dispatch(companyExportFilterXslxSucces(resp)))
            .catch(err => dispatch(companyExportFilterXslxFailure(err)))
        }
    }
}

export default companyExportFilterXslx;
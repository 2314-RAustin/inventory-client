import {companyExportFilterPdfMiddleware} from '../../../../../../services/middleware'
import {companyExportAllPdfMiddleware} from '../../../../../../services/middleware'

export const COMPANY_EXPORT_FILTER_PDF_REQUEST = "COMPANY_EXPORT_FILTER_PDF_REQUEST";
export const COMPANY_EXPORT_FILTER_PDF_SUCCESS = "COMPANY_EXPORT_FILTER_PDF_SUCCESS";
export const COMPANY_EXPORT_FILTER_PDF_FAILURE = "COMPANY_EXPORT_FILTER_PDF_FAILURE";

export const companyExportFilterPdfRequest = (values) => {
    console.log(COMPANY_EXPORT_FILTER_PDF_REQUEST, ' FILTERS: ', values)
    return {
        type:COMPANY_EXPORT_FILTER_PDF_REQUEST,
        payload: 'loading...'
    }
}

export const companyExportFilterPdfSucces = (response) => {
    console.log(COMPANY_EXPORT_FILTER_PDF_SUCCESS, 'RESPUESTA:', response)
    return {
        type:COMPANY_EXPORT_FILTER_PDF_SUCCESS,
        payload: response
    }
}

export const companyExportFilterPdfFailure = (error) => {
    console.log(COMPANY_EXPORT_FILTER_PDF_FAILURE, 'ERROR: ', error)
    return {
        type:COMPANY_EXPORT_FILTER_PDF_FAILURE,
        payload: error
    }
}

const companyExportFilterPdf = (values) => {
    return (dispatch) => {
        dispatch(companyExportFilterPdfRequest(values));
        if(!values){
            companyExportAllPdfMiddleware()
            .then(resp => dispatch(companyExportFilterPdfSucces(resp)))
            .catch(err => dispatch(companyExportFilterPdfFailure(err)))
        } else {
            companyExportFilterPdfMiddleware(values)
            .then(resp => dispatch(companyExportFilterPdfSucces(resp)))
            .catch(err => dispatch(companyExportFilterPdfFailure(err)))
        }
    }
}

export default companyExportFilterPdf;
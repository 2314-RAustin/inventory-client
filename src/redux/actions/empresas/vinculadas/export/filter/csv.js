import {companyExportFilterCsvMiddleware} from '../../../../../../services/middleware'
import {companyExportAllCsvMiddleware} from '../../../../../../services/middleware'

export const COMPANY_EXPORT_FILTER_CSV_REQUEST = "COMPANY_EXPORT_FILTER_CSV_REQUEST";
export const COMPANY_EXPORT_FILTER_CSV_SUCCESS = "COMPANY_EXPORT_FILTER_CSV_SUCCESS";
export const COMPANY_EXPORT_FILTER_CSV_FAILURE = "COMPANY_EXPORT_FILTER_CSV_FAILURE";

export const companyExportFilterCsvRequest = (values) => {
    console.log(COMPANY_EXPORT_FILTER_CSV_REQUEST, ' FILTERS: ', values)
    return {
        type:COMPANY_EXPORT_FILTER_CSV_REQUEST,
        payload: 'loading...'
    }
}

export const companyExportFilterCsvSucces = (response) => {
    console.log(COMPANY_EXPORT_FILTER_CSV_SUCCESS, 'RESPUESTA:', response)
    return {
        type:COMPANY_EXPORT_FILTER_CSV_SUCCESS,
        payload: response
    }
}

export const companyExportFilterCsvFailure = (error) => {
    console.log(COMPANY_EXPORT_FILTER_CSV_FAILURE, 'ERROR: ', error)
    return {
        type:COMPANY_EXPORT_FILTER_CSV_FAILURE,
        payload: error
    }
}

const companyExportFilterCsv = (values) => {
    return (dispatch) => {
        dispatch(companyExportFilterCsvRequest(values));
        if(!values){
            companyExportAllCsvMiddleware()
            .then(resp => dispatch(companyExportFilterCsvSucces(resp)))
            .catch(err => dispatch(companyExportFilterCsvFailure(err)))
        } else {
            companyExportFilterCsvMiddleware(values)
            .then(resp => dispatch(companyExportFilterCsvSucces(resp)))
            .catch(err => dispatch(companyExportFilterCsvFailure(err)))
        }
    }
}

export default companyExportFilterCsv;
export const type = 'login';

const login = object => {
    return {
        type,
        payload: object
    }
}

export default login;
import {createStore, combineReducers, applyMiddleware} from 'redux';
import Login from './reducers/login'
import read_company_response from './reducers/empresas/vinculadas/crud/read';
import create_company_response from './reducers/empresas/vinculadas/crud/create';
import update_company_response from './reducers/empresas/vinculadas/crud/update';
import delete_company_response from './reducers/empresas/vinculadas/crud/delete';
import export_all_csv from './reducers/empresas/vinculadas/export/all/csv';
import export_all_pdf from './reducers/empresas/vinculadas/export/all/pdf';
import export_all_xslx from './reducers/empresas/vinculadas/export/all/xslx';
import export_filter_csv from './reducers/empresas/vinculadas/export/filter/csv';
import export_filter_pdf from './reducers/empresas/vinculadas/export/filter/pdf';
import export_filter_xslx from './reducers/empresas/vinculadas/export/filter/xslx';
import {composeWithDevTools} from 'redux-devtools-extension'
import thunk from 'redux-thunk';

const reducer = combineReducers({
    Login,
    read_company_response,
    create_company_response,
    update_company_response,
    delete_company_response,
    export_all_csv,
    export_all_pdf,
    export_all_xslx,
    export_filter_csv,
    export_filter_pdf,
    export_filter_xslx
})

const store = createStore(reducer, composeWithDevTools(
    applyMiddleware(thunk)
));

export default store;